# dromTask02

## Инициализация
```php
$guzzle = new \GuzzleHttp\Client();
$host = 'example.com';
$client = new \ExampleClient\ExampleClient($guzzle, $host);
```

## Получить список комментариев
```php
$commentClient = new \ExampleClient\Comment\CommentClient($client);
$limit = 30;
$offset = 0;
$list = $commentClient->getListAction($limit, $offset); // Получить первых 30 комментариев

$comments = $list->getItems()
```

## Добавить комментарий
```php
$commentClient = new \ExampleClient\Comment\CommentClient($client);
$name = 'Name';
$text = 'Text';
$comment = \ExampleClient\Comment\DTO\Request\Comment::newComment($name, $text) // Создать ДТО комментария
$commentClient->postAction($comment);
```

## Изменить комментарий
```php
$commentClient = new \ExampleClient\Comment\CommentClient($client);
$id = 1; // id комментария который меняем
$name = 'EditName';
$text = 'editText';
$comment = new \ExampleClient\Comment\DTO\Request\Comment();
$comment // Сэтим данные для изменения
    ->setName($name) // Для изменения названия
    ->setText($text) // Для изменения текста
$commentClient->putAction($id, $comment);
```