<?php

namespace ExampleClient\Classes\DTO;

use JsonSerializable;

class AbstractDTORequest implements JsonSerializable
{
    public function jsonSerialize(): array
    {
        return array_filter(get_object_vars($this), static function ($value) {
            return null !== $value;
        });
    }
}