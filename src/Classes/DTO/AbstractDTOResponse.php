<?php

namespace ExampleClient\Classes\DTO;

class AbstractDTOResponse
{
    public function __construct(array $response)
    {
        $properties = array_keys(get_class_vars(get_called_class()));

        foreach ($response as $key => $value) {
            if (in_array($key, $properties, true)) {
                $this->{$key} = $value;
            }
        }
    }
}