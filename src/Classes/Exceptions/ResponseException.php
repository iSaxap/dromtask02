<?php

namespace ExampleClient\Classes\Exceptions;

use RuntimeException;

class ResponseException extends RuntimeException
{
}