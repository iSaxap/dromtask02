<?php

namespace ExampleClient\Classes\Interfaces;

use GuzzleHttp\Client;

interface ApiClientInterface
{
    public function request(string $method, string $uri, $body = null, array $opts = []): array;

    public function clearAuthToken(): void;

    public function getHost(): string;
}