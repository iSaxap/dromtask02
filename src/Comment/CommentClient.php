<?php

namespace ExampleClient\Comment;

use ExampleClient\AbstractClient;
use ExampleClient\Comment\DTO\Request\Comment;
use ExampleClient\Comment\DTO\Response\Comment as ResponseComment;
use ExampleClient\Comment\DTO\Response\ListComment;

class CommentClient extends AbstractClient
{
    public function getListAction(int $limit = self::DEFAULT_LIMIT, int $offset = 0): ListComment
    {
        $query = [
            'query' => [
                'limit' => $limit,
                'offset' => $offset
            ]
        ];
        $uri = '/comments';

        $response = $this->get($uri, $query);

        return new ListComment($response);
    }

    public function postAction(Comment $comment): ResponseComment
    {
        $uri = '/comments';
        $response = $this->send($uri, $comment);

        return new ResponseComment($response);

    }

    public function putAction(int $id, Comment $comment): ResponseComment
    {
        $uri = '/comments/' . $id;
        $response = $this->update($uri, $comment);

        return new ResponseComment($response);
    }

}