<?php

namespace ExampleClient\Comment\DTO\Request;

use ExampleClient\Classes\DTO\AbstractDTORequest;

class Comment extends AbstractDTORequest
{
    protected string $name;

    protected string $text;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Comment
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Comment
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    public static function newComment(string $name, string $text): Comment
    {
        $comment = new Comment();
        $comment->setName($name)->setText($text);
        return $comment;
    }
}