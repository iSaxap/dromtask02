<?php

namespace ExampleClient\Comment\DTO\Response;

use ExampleClient\Classes\DTO\AbstractDTOResponse;

class ListComment extends AbstractDTOResponse
{
    protected int $count;

    /**
     * @var Comment[]
     */
    protected array $items = [];

    protected int $limit;

    protected int $offset;

    public function __construct(array $response)
    {
        if (!empty($response['items'])) {
            $this->fillItems((array) $response['items']);
        }
        unset($response['items']);
        parent::__construct($response);
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Comment[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    private function fillItems(array $items): void
    {
        foreach ($items as $item) {
            $item = new Comment($item);
            $this->addItem($item);
        }
    }

    protected function addItem(Comment $item): void
    {
        $this->items[] = $item;
    }
}