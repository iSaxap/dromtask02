<?php

namespace ExampleClient\Comment\DTO\Response;

use ExampleClient\Classes\DTO\AbstractDTOResponse;

class Comment extends AbstractDTOResponse
{
    protected int $id;

    protected string $name;

    protected string $text;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }


}