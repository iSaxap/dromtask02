<?php

namespace ExampleClient;

use ExampleClient\Classes\Interfaces\ApiClientInterface;

class AbstractClient
{
    public const GET = 'GET';

    public const POST = 'POST';

    public const PUT = 'PUT';

    public const DELETE = 'DELETE';

    public const DEFAULT_LIMIT = 50;

    private ApiClientInterface $client;

    public function __construct(ApiClientInterface $client)
    {
        $this->client = $client;
    }

    protected function get(string $uri, array $opts = []): array
    {
        return $this->client->request(self::GET, $uri, null, $opts);
    }

    protected function send(string $uri, $body = null, array $opts = []): array
    {
        return $this->client->request(self::POST, $uri, $body, $opts);
    }

    protected function update(string $uri, $body = null, array $opts = []): array
    {
        return $this->client->request(self::PUT, $uri, $body, $opts);
    }

    protected function delete(string $uri, array $opts = []): array
    {
        return $this->client->request(self::DELETE, $uri, null , $opts);
    }
}