<?php

namespace ExampleClient;

use ExampleClient\Classes\Exceptions\ResponseException;
use ExampleClient\Classes\Interfaces\ApiClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class ExampleClient implements ApiClientInterface
{
    private string $host;
    private string $token;
    private Client $client;

    public function __construct(Client $client, string $host)
    {
        $this->client = $client;
        $this->host = $host;
    }

    /**
     * @throws ResponseException
     * @throws GuzzleException
     */
    public function request(string $method, string $uri, $body = null, array $opts = []): array
    {
        $options = [
            'headers' => [
                'Content-type' => 'application/json',
                'AUTH-TOKEN' => $this->token ?? $this->getAuthToken(),
            ],
        ];

        if ($body) {
            $options['body'] = json_encode($body);
        }

        if ($opts) {
            $options = array_merge_recursive($options, $opts);
        }

        return $this->processResponse($this->client->request($method, $this->getHost() . $uri, $options));
    }

    public function clearAuthToken(): void
    {
        unset($this->token);
    }


    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Авторизация
     * Заглушка авторизации
     */
    private function getAuthToken(): string
    {
        $this->token = "token";
        return $this->token;
    }

    private function processResponse(ResponseInterface $response): array
    {
        if (!$result = json_decode($response->getBody()->getContents(), true)) {
            throw new ResponseException('Получен невалидный json');
        }

        if (isset($result['errors'])) {
            $encodeErrors = json_encode($result['errors']) ?: 'Невозможно преобразовать массив';

            $error = (isset($result['errors'][0]) && is_string($result['errors'][0])) ? $result['errors'][0] : $encodeErrors;

            throw new ResponseException($error);
        }

        return $result;
    }
}