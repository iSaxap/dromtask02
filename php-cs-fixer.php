<?php

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setCacheFile('.php_cs.cache')
    ->setRules([
        'array_syntax' => ['syntax' => 'short'],
    ])
    ;