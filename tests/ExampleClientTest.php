<?php

use ExampleClient\Classes\Exceptions\ResponseException;
use ExampleClient\ExampleClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ExampleClientTest extends TestCase
{
    public function optionsProvider(): array
    {
        return [
            [[
                'headers' => [
                    'Content-type' => 'application/json',
                    //TODO Заглушка
                    'AUTH-TOKEN' => 'token',
                ],
                'body' => '"body"',
                'query' => ['foo' => 'bar']
            ],
            [
                'method' => 'POST',
                'uri' => '/test',
                'body' => 'body',
                'query' => ['query' => ['foo' => 'bar']]
            ],
                '{"foo":"bar"}'
            ]
        ];
    }

    /**
     * @dataProvider optionsProvider
     * @throws GuzzleException
     */
    public function testRequest(array $option, array $request, string $response): void
    {
        /** @var Client|MockObject $client */
        $client = $this->createMock(Client::class);

        $response = new Response(200, [], $response);

        $client->expects($this->once())
            ->method('request')
            ->with($this->equalTo('POST'), $this->equalTo('Example.com/test'), $this->equalTo($option))
            ->willReturn($response);

        $response = (new ExampleClient($client, 'Example.com'))->request($request['method'], $request['uri'], $request['body'], $request['query']);

        $this->assertSame($response['foo'], 'bar');
    }

    /**
     * @dataProvider optionsProvider
     * @throws GuzzleException
     */
    public function testNegativeRequest(array $option, array $request, string $response): void
    {
        /** @var Client|MockObject $client */
        $client = $this->createMock(Client::class);

        $response = new Response(200, [], '');

        $client->expects($this->once())
            ->method('request')
            ->with($this->equalTo('POST'), $this->equalTo('Example.com/test'), $this->equalTo($option))
            ->willReturn($response);

        $this->expectException(ResponseException::class);
        $this->expectExceptionMessage("Получен невалидный json");

        (new ExampleClient($client, 'Example.com'))->request($request['method'], $request['uri'], $request['body'], $request['query']);
    }

    public function testGetHost(): void
    {
        /** @var Client|MockObject $client */
        $client = $this->createMock(Client::class);

        $response = (new ExampleClient($client, 'Example.com'));

        $this->assertSame($response->getHost(), 'Example.com');
    }
}