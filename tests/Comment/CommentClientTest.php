<?php


use ExampleClient\Classes\Interfaces\ApiClientInterface;
use ExampleClient\Comment\CommentClient;
use ExampleClient\Comment\DTO\Request\Comment;
use ExampleClient\ExampleClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CommentClientTest extends TestCase
{
    public function listCommentProvider(): array
    {
        $list = [
            'count' => 2,
            'items' => [
                ['id' => 1, 'name' => 'testName1', 'text' => 'testText1'],
                ['id' => 2, 'name' => 'testName2', 'text' => 'testText2'],
            ],
            'limit' => 50,
            'offset' => 0
        ];
        $option = [
            'query' => [
                'limit' => 50,
                'offset' => 0
            ]
        ];
        return [[$list, $option]];
    }

    public function commentProvider(): array
    {
        $comment = ['name' => 'testName1', 'text' => 'testText1'];
        $response = ['id' => 1, 'name' => 'testName1', 'text' => 'testText1'];
        return [[$comment, $response]];
    }

    /**
     * @dataProvider listCommentProvider
     */
    public function testGetListAction(array $response, array $option): void
    {
        /** @var ApiClientInterface|MockObject $client */
        $client = $this->createMock(ExampleClient::class);

        $client->expects($this->once())
            ->method('request')
            ->with($this->equalTo('GET'), $this->equalTo('/comments'), $this->equalTo(null), $this->equalTo($option))
            ->willReturn($response);

        $responseDTO = (new CommentClient($client))->getListAction();
        $this->assertSame($response['count'], $responseDTO->getCount());
        $this->assertSame($response['count'], count($responseDTO->getItems()));
        $this->assertSame($response['items'][0]['id'], ($responseDTO->getItems()[0])->getId());
        $this->assertSame($response['items'][0]['name'], ($responseDTO->getItems()[0])->getName());
        $this->assertSame($response['items'][0]['text'], ($responseDTO->getItems()[0])->getText());
        $this->assertSame($response['limit'], $responseDTO->getLimit());
        $this->assertSame($response['offset'], $responseDTO->getOffset());
    }

    /**
     * @dataProvider commentProvider
     */
    public function testPostAction($request, $response): void
    {
        /** @var ApiClientInterface|MockObject $client */
        $client = $this->createMock(ExampleClient::class);

        $requestDTO = Comment::newComment(...array_values($request));

        $client->expects($this->once())
            ->method('request')
            ->with($this->equalTo('POST'), $this->equalTo('/comments'), $this->equalTo($requestDTO))
            ->willReturn($response);

        $responseDTO = (new CommentClient($client))->postAction($requestDTO);

        $this->assertSame($requestDTO->getName(), $responseDTO->getName());
        $this->assertSame($requestDTO->getText(), $responseDTO->getText());
        $this->assertSame($response['id'], $responseDTO->getId());

    }

    /**
     * @dataProvider commentProvider
     */
    public function testPutAction($request, $response): void
    {
        /** @var ApiClientInterface|MockObject $client */
        $client = $this->createMock(ExampleClient::class);

        $requestDTOText = new Comment();
        $requestDTOText->setText($request['text']);

        $id = 1;

        $client->expects($this->once())
            ->method('request')
            ->with($this->equalTo('PUT'), $this->equalTo("/comments/{$id}"), $this->equalTo($requestDTOText))
            ->willReturn($response);

        $responseDTO = (new CommentClient($client))->putAction($id, $requestDTOText);

        $this->assertSame($requestDTOText->getText(), $responseDTO->getText());
    }

    public function testCommentDTO(): void
    {
        $dto = new Comment();
        $dto->setName('name')->setText('text');

        $this->assertSame(json_encode($dto), '{"name":"name","text":"text"}');
    }
}